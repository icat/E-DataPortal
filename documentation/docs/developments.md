# Developments

- [Tests](#tests)
- [Code quality](#code-quality)
- [Automatic fixing and formatting](#automatic-fixing-and-formatting)
- [Editor integration](#editor-integration)
- [Widgets](#Widgets)
- [ResponsiveTable](#responsivetable)
- [Loader](#loader)

## Tests

Run:

```
npm test
```

## Code quality 🔎

- `npm run lint` - run linting and code formatting commands
- `npm run lint:eslint` - lint all JS files with ESLint (this command will report warnings as errors)
- `npm run lint:prettier` - check that all files have been formatted with Prettier

### Automatic fixing and formatting

- `npm run lint:eslint -- --fix` - auto-fix linting issues
- `npm run lint:prettier -- --write` - format all files with Prettier

### Editor integration

Most editors support fixing and formatting files automatically on save. The configuration for VSCode is provided out of
the box, so all you need to do is install the recommended extensions.

## Widgets

### ResponsiveTable

This widget is based on react-bootstrap-table2 (https://react-bootstrap-table.github.io/react-bootstrap-table2/) but adds responsiveness.

Columns can now be configured based on the size of the device as bootstrap does (xs, sm, md, lg)

Configuration:

```js
const columns = [
  {
    text: 'Proposal',
    dataField: 'name',
    headerStyle: (column, colIndex) => {
      return { width: '50%', textAlign: 'center' };
    },
    responsiveHeaderStyle: {
      xs: { width: '20%', textAlign: 'center' },
      sm: { width: '50%', textAlign: 'center' },
      md: { width: '70%', textAlign: 'center' },
      lg: { width: '90%', hidden: true, textAlign: 'center' },
    },
  },
];

var pageOptions = {
  showTotal: true,
  hidePageListOnlyOnePage: true,
  sizePerPageList: [
    {
      text: '5',
      value: 5,
    },
    {
      text: '10',
      value: 10,
    },
    {
      text: 'All',
      value: data.length,
    },
  ],
};

<ResponsiveTable data={data} columns={columns} pageOptions={pageOptions} />;
```

If there is no responsiveHeaderStyle then headerStyle will be used as default. If there is no the used screen size (xs, sm, md, lg) configured in the responsiveHeaderStyle attribute then headerStyle will be used by default.

You may specify which field of the table is unique by passing a `keyField` prop. If you omit it, the default is `"id"`.

### Loader

It informs users that data are still been loaded

```js
// With default loading message
<Loader />
```

```js
// With custom loading message and space above/below
<Loader message="Loading authors..." spacedOut />
```

```js
// When used inside a Bootstrap `Panel`
<Loader message="Loading datasets..." inPanel />
```
