# Installation

Make sure the dependencies mentioned above are installed and properly configured.

1. Clone or download the project from [GitLab](https://gitlab.esrf.fr/icat/E-DataPortal)
1. Install the dependencies

   ```
   npm install
   ```

1. Start the app

   ```
   npm start
   ```
