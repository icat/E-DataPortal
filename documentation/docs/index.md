# Data Portal

The Data Portal is a frontend available as a [Node.js](https://nodejs.org/en/) package. It is based on the [React](https://reactjs.org/) framework. Through a web interface, this application provides:

- a user authentication portal
- a GUI for scientific metadata management
- a GUI for the electronic logbook which can complement data
- experimental data downloading feature
- manual DOI minting feature

Currently, datahub depends on:

- ICAT+ package. ICAT+ is the backend which stores logbook events
- [ICAT](https://icatproject.org) metadata catalogue. ICAT stores metadata and handles the user authentication mechanism
