# Configuration

The data portal configuration is spread among different files located in <kbd>src/config</kbd>. These files are:

- `icat.js` - configure access to metadata catalogue
- `icatPlus.js` - configure access to ICAT+ server
- [`techniques.js`](##techniques) - configure known techniques
- [`ui.js`](##UI) - general UI configuration

Some of the configuration options in these files are stored in [environment variables](#environment-variables).

## Environment variables

Create React App loads environment variables from [environment files](https://create-react-app.dev/docs/adding-custom-environment-variables/#what-other-env-files-can-be-used) (<kbd>.env</kbd>, <kbd>.env.test</kbd>, etc.) according to the Node environment (`NODE_ENV`).
File <kbd>.env</kbd> is loaded in every environment with the lowest priority and is therefore used to declare default values and development fallbacks.

The following portal configuration variables are declared in <kbd>.env</kbd>:

- `REACT_APP_ICATPLUS_URL` - the URL of the ICAT+ server (used in <kbd>src/config/icatPlus.js</kbd>)
- `REACT_APP_SSO_AUTH_ENABLED` - whether to allow users to sign-in to ICAT with SSO (used in <kbd>src/config/icat.js</kbd>)
- `REACT_APP_ANONYMOUS_AUTH_ENABLED` - whether to allow users to sign-in to ICAT as anonymous (used in <kbd>src/config/icat.js</kbd>)
- `REACT_APP_DB_AUTH_ENABLED` - whether to allow users to sign-in to ICAT with database credentials (used in <kbd>src/config/icat.js</kbd>)

In development, you can override any of these variables by creating a file called `.env.local`. This file is ignored from version control.

## Techniques

The file [techniques.js](https://gitlab.esrf.fr/icat/E-DataPortal/-/blob/master/src/config/techniques.js) complements short named techniques as stored in ICAT metadata catalogue. It maps a short name to a description and display settings.

To add a new technique, use the following object to fully define the technique.

```js
/** Object defining the new technique */
{
/** Full name of the technique */
name: "Ptychography",
/** Short name of the technique as stored in ICAT */
shortname: "PTYCHO",
/** Full description of the technique */
description: "Ptychography is a technique invented by Walter Hoppe that aims to solve the diffraction-pattern phase problem by interfering adjacent Bragg reflections coherently and thereby determine their relative phase.",
/** Font color */
color: "#97E0FE"
},
```

## UI

See [ui.js](https://gitlab.esrf.fr/icat/E-DataPortal/-/blob/master/src/config/ui.js)
