import React from 'react';
import { render } from '@testing-library/react';
import { CacheProvider } from 'rest-hooks';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import reducers from './reducers';
import App from './App';

export function renderApp(ui, options = {}) {
  const { initialState, initialRoute = '/', ...renderOptions } = options;

  const store = createStore(
    reducers,
    applyMiddleware(thunk, promise),
    initialState
  );

  function Wrapper() {
    return (
      <CacheProvider>
        <Provider store={store}>
          <MemoryRouter initialEntries={[initialRoute]}>
            <App />
          </MemoryRouter>
        </Provider>
      </CacheProvider>
    );
  }

  return render(ui, { wrapper: Wrapper, ...renderOptions });
}

// Re-export everything to allow importing RTL functions through `test-utils`
export * from '@testing-library/react';
