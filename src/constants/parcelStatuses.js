/** List of actions sorted chronologically */
export const STATUS = Object.freeze({
  CREATED: 'CREATED',
  SCHEDULED: 'SCHEDULED',
  READY: 'READY',
  SENT: 'SENT',
  STORES: 'STORES',
  BEAMLINE: 'BEAMLINE',
  BACK_STORES: 'BACK_STORES',
  BACK_USER: 'BACK_USER',
  DESTROYED: 'DESTROYED',
});

export const STATUS_DEFS = {
  [STATUS.CREATED]: {
    desc: 'Parcel has been created',
    label: 'CREATED',
    icon: 'faExternalLinkAlt',
    editable: true,
    inactiveParcel: false,
    sendable: false,
    allowedActions: () => [STATUS.SCHEDULED],
  },
  [STATUS.SCHEDULED]: {
    desc: 'Parcel has been associated with an experimental session',
    label: 'SCHEDULED',
    icon: 'faCalendar',
    editable: true,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'SCHEDULED',
      desc: 'Associate the parcel with an experimental session',
    },
    allowedActions: (parcel) =>
      parcel.content.length > 0 ? [STATUS.SENT] : [],
  },
  [STATUS.SENT]: {
    desc: 'Sent to facility',
    label: 'SENT',
    icon: 'faEnvelope',
    editable: false,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'Mark as SENT',
      desc:
        '<p>Notify that the parcel has been shipped to the facility.</p><p><b>Before</b> sending your parcel to the ESRF, <b>you must</b>:</p><ol><li>Download and print your shipping label</li><li>Affix the "to ESRF" part to your parcel</li> <li>Insert the return address label in your parcel</li><li>Complete the paperwork for your courier company - remember to request the <b>courier company return label</b> and put it in your parcel otherwise it will not be returned to you.</li><li><b>Update the status</b> of your parcel to <b>Sent</b>, once you have sent it.</ol><p>Please note that any parcel <b><u>without</u></b> a label will <b><u>not be accepted</u></b> by the ESRF Stores.</p>',
      requiresInput: true,
    },
    allowedActions: () => [STATUS.STORES],
  },
  [STATUS.STORES]: {
    desc: 'Arrived at facility stores',
    label: 'STORES',
    icon: 'faHome',
    editable: false,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'Mark as RECEIVED IN STORES',
      desc: 'Notify that the parcel has arrived at the stores',
      safety: true,
    },
    allowedActions: () => [STATUS.BEAMLINE],
  },
  [STATUS.BEAMLINE]: {
    desc: 'Arrived at the beamline',
    label: 'BEAMLINE',
    icon: 'faThumbsUp',
    editable: false,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'Mark as RECEIVED AT BEAMLINE',
      desc: 'Notify that the parcel has arrived at the beamline',
    },
    allowedActions: (parcel) => {
      return parcel.returnAddress
        ? [STATUS.BACK_STORES]
        : [STATUS.BACK_STORES, STATUS.DESTROYED];
    },
  },
  [STATUS.BACK_STORES]: {
    desc: 'Back to facility stores',
    label: 'BACK IN STORES',
    icon: 'faHome',
    editable: false,
    inactiveParcel: true,
    sendable: true,
    action: {
      label: 'Mark as RETURNED TO STORES',
      desc: 'Notify that the parcel has been returned to the stores',
      safety: true,
    },
    allowedActions: (parcel) => [
      parcel.returnAddress ? STATUS.BACK_USER : STATUS.DESTROYED,
    ],
  },
  [STATUS.BACK_USER]: {
    desc: 'Sent back to return address',
    label: 'SENT BACK TO USER',
    icon: 'faUser',
    editable: false,
    inactiveParcel: true,
    sendable: true,
    action: {
      label: 'Mark as SENT BACK TO THE USER',
      desc: 'Notify that the parcel has been shipped back to the user',
    },
    allowedActions: () => [],
  },
  [STATUS.DESTROYED]: {
    desc: 'Parcel destroyed',
    label: 'DESTROYED',
    icon: 'faTimesCircle',
    editable: false,
    inactiveParcel: true,
    sendable: true,
    action: {
      label: 'Mark as DESTROYED',
      desc: 'Notify that the parcel has been destroyed',
      safety: true,
    },
    allowedActions: () => [],
  },
};
