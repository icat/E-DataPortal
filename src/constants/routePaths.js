export const MY_DATA_PATH = '/investigations';
export const CLOSED_DATA_PATH = '/closed';
export const OPEN_DATA_PATH = '/public';
export const BEAMLINE_PATH = '/beamline/';
export const CAMERA_PATH = '/camera';
export const WELCOME_PATH = '/welcome';
export const LANDING_PAGE_PATH = '/doi';
