export const ES_RESOURCE_TYPE = 'Experiment Session';
export const DC_RESOURCE_TYPE = 'Datacollection';
export const DATASET = 'Dataset';
export const OPEN_ACCESS = 'Open access';
export const RESTRICTED_ACCESS = 'Restricted access';
export const NO_LINK = 'noLink';
