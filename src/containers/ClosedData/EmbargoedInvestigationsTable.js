import React from 'react';
import InvestigationTable from '../../components/Investigation/InvestigationTable';

function EmbargoedInvestigationsTable() {
  return <InvestigationTable filter="embargoed" />;
}

export default EmbargoedInvestigationsTable;
