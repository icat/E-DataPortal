import React from 'react';

import DatasetWidgetFactory from '../Dataset/Widgets/DatasetWidgetFactory';
import InlineDatasetTable from './InlineDatasetTable';
import { useSelector } from 'react-redux';
import Table from 'react-bootstrap/Table';

const getHeaderRowSpanded = (text) => {
  return (
    <th rowSpan="2" style={{ verticalAlign: 'middle' }}>
      {text}
    </th>
  );
};

function BioSaxsInlineDatasetTable(props) {
  const { datasets, processedDatasets } = props;
  const sessionId = useSelector((state) => state.user.sessionId);

  return (
    <>
      <div style={{ margin: 20 }}>
        <InlineDatasetTable {...props}>
          <Table responsive striped="columns" bordered hover>
            <thead>
              <tr>
                {getHeaderRowSpanded('Run')}
                {getHeaderRowSpanded('Sample')}
                {getHeaderRowSpanded('Dataset')}
                <th colSpan="2">Frames</th>
                <th colSpan="3">Guinier</th>
                <th colSpan="3">BIFT</th>
                <th colSpan="2">Porod</th>
                {getHeaderRowSpanded('Integration')}
                {getHeaderRowSpanded('Scattering')}
                {getHeaderRowSpanded('Kratky')}
                {getHeaderRowSpanded('Density')}
                {getHeaderRowSpanded('Guinier')}
              </tr>
              <tr>
                <th>Avg/Total</th>
                <th>Time</th>
                <th>Rg</th>
                <th>Points</th>
                <th>IO</th>
                <th>Rg</th>
                <th>Total</th>
                <th>
                  D<sub>max</sub>
                </th>
                <th>Volume</th>
                <th>MM vol. est.</th>
              </tr>
            </thead>
            <tbody>
              {datasets.map((ds) => (
                <DatasetWidgetFactory
                  dataset={ds}
                  sessionId={sessionId}
                  processedDatasets={processedDatasets}
                />
              ))}
            </tbody>
          </Table>
        </InlineDatasetTable>
      </div>
    </>
  );
}

export default BioSaxsInlineDatasetTable;
