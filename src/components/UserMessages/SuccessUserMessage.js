import React, { useState } from 'react';
import { Alert } from 'react-bootstrap';

export function SuccessUserMessage(props) {
  const { text, isDismissible } = props;
  const [show, setShow] = useState(true);
  if (show) {
    return (
      <Alert
        variant="success"
        onClose={() => setShow(false)}
        dismissible={isDismissible ? isDismissible : false}
      >
        <p>{text}</p>
      </Alert>
    );
  }
  return <></>;
}
