import React, { useState } from 'react';
import { Alert } from 'react-bootstrap';
import { STATUS, STATUS_DEFS } from '../../constants/parcelStatuses';
import DangerousGoodsModal from './DangerousGoodsModal';
import StatusButton from './StatusButton';
import styles from './ParcelStatusButtons.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

function ParcelStatusButtons(props) {
  const { parcel, updateStatus, showSafetyActions } = props;
  const { status: currentStatus } = parcel;

  const allowedActions = showSafetyActions
    ? STATUS_DEFS[currentStatus].allowedActions(parcel)
    : STATUS_DEFS[currentStatus]
        .allowedActions(parcel)
        .filter((status) => !STATUS_DEFS[status].action.safety);

  const editableEmptyParcel =
    parcel.content.length === 0 && STATUS_DEFS[currentStatus].editable;

  const [nextStatus, setNextStatus] = useState();
  const [isProcessing, setProcessing] = useState(false);
  const [isModalElementDisplayed, setIsModalElementDisplayed] = useState(
    nextStatus === STATUS.SENT
  );

  async function onClick(status) {
    if (STATUS_DEFS[status].action.requiresInput) {
      setIsModalElementDisplayed(true);
      return setNextStatus(status);
    }

    setProcessing(true);
    await updateStatus(status);
    setProcessing(false);
  }

  const modalProps = {
    isProcessing,
    setShow: (show) => {
      setIsModalElementDisplayed(show);
    },
    onConfirm: async (values) => {
      setProcessing(true);
      await updateStatus(nextStatus, values);
      setProcessing(false);
      setNextStatus(undefined);
      setIsModalElementDisplayed(false);
    },
    onCloseModal: () => {
      setNextStatus(undefined);
      setIsModalElementDisplayed(false);
    },
  };

  return (
    <div className={styles.wrapper}>
      {isModalElementDisplayed && <DangerousGoodsModal {...modalProps} />}

      {allowedActions.length > 0 && (
        <div className={styles.buttonGroup}>
          <div className={styles.label}>Actions:</div>
          {allowedActions.map((status) => (
            <div className={styles.button} key={status}>
              <StatusButton
                onClick={() => onClick(status)}
                status={status}
                isProcessing={isProcessing}
              />
            </div>
          ))}
        </div>
      )}
      {editableEmptyParcel && (
        <Alert variant="light">
          <FontAwesomeIcon icon={faInfoCircle} style={{ marginRight: 10 }} />
          An empty parcel cannot be set as {STATUS_DEFS[STATUS.SENT].label}.
        </Alert>
      )}
    </div>
  );
}

export default ParcelStatusButtons;
