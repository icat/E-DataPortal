import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { STATUS_DEFS } from '../../constants/parcelStatuses';
import { fas as icons } from '@fortawesome/free-solid-svg-icons';

function StatusDisplay(props) {
  const { status, ref, action, ...triggerHandler } = props;
  const statusDef = STATUS_DEFS[status];
  const statusLabel = statusDef
    ? action
      ? statusDef.action.label
      : statusDef.label
    : status;
  return (
    <span ref={ref} {...triggerHandler}>
      <FontAwesomeIcon
        icon={statusDef ? icons[statusDef.icon] : undefined}
        style={{ marginRight: 10 }}
      />{' '}
      {statusLabel}
    </span>
  );
}

export default StatusDisplay;
