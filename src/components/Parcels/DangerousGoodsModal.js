import React, { useState } from 'react';
import { Alert, Button, Modal, Form } from 'react-bootstrap';

function DangerousGoodsModal(props) {
  const { onConfirm, onCloseModal, setShow } = props;

  const [certify, setCertify] = useState(false);
  const [trackingNumber, setTrackingNumber] = useState('');
  const [forwarderName, setForwarderName] = useState('');
  const [containsDangerousGoods, setContainsDangerousGoods] = useState(
    undefined
  );

  const filled = certify && containsDangerousGoods !== undefined;

  return (
    <Modal show aria-labelledby="parcel-modal-title" onHide={onCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title id="parcel-modal-title">
          Certify parcel content
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form.Check
          type="switch"
          id="switch-herby"
          checked={certify}
          label={'I hereby certify that:'}
          onChange={(e) => {
            setCertify(e.currentTarget.checked);
          }}
        />
        <ul>
          <li>The parcel does not contains any radioactive sources.</li>
          <li>
            The parcel contains only samples that have been declared in the
            Proposal/ Sample-sheet form(s) and that have been approved by the
            ESRF safety group.
          </li>
          <li>
            The parcel will be sent according to national and international
            transport regulations and customs regulations, with support of my
            local safety and logistics referents, where possible.
          </li>
          <li>
            I acknowledge that in no event shall the ESRF be held liable for the
            loss of (and any damage caused to) any content of the parcel sent by
            the user.
          </li>
        </ul>
        <Form>
          <Form.Group className="mb-3" controlId="tracking-number">
            <Form.Label>Tracking number</Form.Label>
            <Form.Control
              type="text"
              placeholder="Tracking number"
              value={trackingNumber}
              onChange={(e) => setTrackingNumber(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="tracking-number">
            <Form.Label>Forwarder name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Forwarder name"
              value={forwarderName}
              onChange={(e) => setForwarderName(e.target.value)}
            />
          </Form.Group>
        </Form>
        <h4>Dangerous goods</h4>
        <p>
          This declaration is <strong>NOT</strong> replacing the necessary
          parcel content declaration paperwork requested by the transporter &
          customs.
        </p>
        <Alert
          style={{
            marginTop: '1rem',
          }}
          variant="warning"
        >
          It is your responsibility to ensure that the parcel content
          declaration is compliant with international regulations and that the
          appropriate paperworks are provided to the corresponding authorities
          in due time.
        </Alert>
        <p>
          Please consult the safety referent of your Laboratory/Company or the
          Transport Company to help you on this matter if you have any doubt.
        </p>
        <Form.Check
          type="radio"
          label="Yes, this parcel contains dangerous goods"
          name="dangerous-goods"
          id="dangerous-goods-yes"
          checked={containsDangerousGoods === true}
          onChange={() => setContainsDangerousGoods(true)}
        />
        <Form.Check
          type="radio"
          label="No, I certify that this parcel does not contain dangerous goods in regard of International regulations."
          name="dangerous-goods"
          id="dangerous-goods-no"
          checked={containsDangerousGoods === false}
          onChange={() => setContainsDangerousGoods(false)}
        />
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={() => setShow(false)}>Cancel</Button>
        <Button
          disabled={!filled}
          onClick={() =>
            onConfirm({
              containsDangerousGoods,
              shippingTrackingInformation: {
                trackingNumber,
                forwarder: forwarderName,
              },
            })
          }
        >
          Mark as SENT
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default DangerousGoodsModal;
