import React from 'react';
import UserMessage from '../UserMessage';
import OverlayBox from './OverlayBox';

function ErrorMessageOverlayBox(props) {
  const { errorMessage, onClose } = props;
  return (
    <OverlayBox
      open={!!errorMessage}
      onClose={() => onClose()}
      classNames={{
        modal: 'userMessageModalClass',
        closeButton: 'userMessageCloseButton',
      }}
    >
      {errorMessage && <UserMessage type="error" message={errorMessage} />}
    </OverlayBox>
  );
}

export default ErrorMessageOverlayBox;
