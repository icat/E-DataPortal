import React from 'react';
import { getEventIcon } from '../../../helpers/eventHelpers';

function CategoryIcon(props) {
  return (
    <div className="pull-left" style={{ paddingRight: 8 }}>
      {getEventIcon(props.category, '14')}
    </div>
  );
}

export default CategoryIcon;
