import React from 'react';
import { Card } from 'react-bootstrap';

export function AbstractWidget(props) {
  const { descriptions } = props;

  function getAbstract(descriptions) {
    if (descriptions) {
      const abstractObject = descriptions.find((item) => {
        return item.descriptionType.toLowerCase() === 'abstract';
      });

      if (abstractObject) {
        return abstractObject.description;
      }
    }
    return undefined;
  }

  const abstract = getAbstract(descriptions);

  return (
    <>
      <Card style={{ marginBottom: 12, borderStyle: 'none' }}>
        <Card.Body style={{ padding: 0 }}>
          <Card.Text>
            <pre
              style={{
                whiteSpace: 'pre-wrap',
                wordWrap: 'break-word',
                fontFamily: 'inherit',
                fontSize: '0.8rem',
              }}
            >
              {abstract}
            </pre>
          </Card.Text>
        </Card.Body>
      </Card>
    </>
  );
}
