/**
 * Function used to mock the app UI configuration for tests
 */
export default function getUIConfiguration(config) {
  return {
    __esModule: true,
    default: {
      status: {
        offline: {
          enabled: false,
          message: "Sorry, we're down for scheduled maintenance right now.",
        },
      },
      menu: {
        applicationTitle: 'Data Portal',
        isSearchVisible: true,
        isClosedDataVisible: true,
        isOpenDataVisible: true,
        isMySelectionVisible: true,
      },
      loginForm: {
        header: "Sign in with your ESRF's user account",
      },
      investigationContainer: {
        isDatasetListVisible: true,
      },
      logbook: {
        EVENTS_PER_PAGE: config.EVENTS_PER_PAGE ? config.EVENTS_PER_PAGE : 100,
        EVENTS_PER_DOWNLOAD: config.EVENTS_PER_DOWNLOAD
          ? config.EVENTS_PER_DOWNLOAD
          : 300,
        SORT_EVENTS_BY: config.SORT_EVENTS_BY ? config.SORT_EVENTS_BY : '_id',
        SORTING_ORDER: config.SORTING_ORDER ? config.SORTING_ORDER : -1,
        SINGLE_EDITOR_MIN_HEIGHT: config.SINGLE_EDITOR_MIN_HEIGHT
          ? config.SINGLE_EDITOR_MIN_HEIGHT
          : '200px',
        SINGLE_EDITOR_MAX_HEIGHT: config.SINGLE_EDITOR_MAX_HEIGHT
          ? config.SINGLE_EDITOR_MAX_HEIGHT
          : '800px',
        DUAL_EDITOR_MIN_HEIGHT: config.DUAL_EDITOR_MIN_HEIGHT
          ? config.DUAL_EDITOR_MIN_HEIGHT
          : '270px',
        DUAL_EDITOR_MAX_HEIGHT: config.DUAL_EDITOR_MAX_HEIGHT
          ? config.DUAL_EDITOR_MAX_HEIGHT
          : '270px',
        DEFAULT_TAG_COLOR: config.DEFAULT_TAG_COLOR
          ? config.DEFAULT_TAG_COLOR
          : '#a6bded',
        AUTOREFRESH_EVENTLIST:
          config.AUTOREFRESH_EVENTLIST === false
            ? config.AUTOREFRESH_EVENTLIST
            : true,
        AUTOREFRESH_DELAY: config.AUTOREFRESH_DELAY
          ? config.AUTOREFRESH_DELAY
          : 10000,
      },
      footer: {
        text: 'European Synchrotron Radiation Facility',
      },
    },
  };
}
