//import { getUserSpecificSelectionFilters, getSelectionFiltersForMongoQuery, getSelectionFiltersBySearchCriteria } from '../containers/Logbook/SelectionFilterHelper';
import getUIConfiguration from '../uiConfig';

require('it-each')({ testPerIteration: true });

const resources = require('./resources/selectionFilterHelper.resource');

describe('selectionFilterHelper', () => {
  describe('getUserSpecificSelectionFilters', () => {
    describe('handle errors', () => {
      it.each(
        resources.getUserSpecificSelectionFilters,
        '[note: %s ]',
        ['description'],
        (element, next) => {
          const getUserSpecificSelectionFilters = require('../../containers/Logbook/SelectionFilterHelper')
            .getUserSpecificSelectionFilters;
          expect(getUserSpecificSelectionFilters(element.criteria)).toEqual(
            element.expected
          );
          next();
        }
      );
    });
  });

  describe('getSelectionFiltersForMongoQuery', () => {
    it.each(
      resources.getSelectionFiltersForMongoQuery,
      '[note: %s ]',
      ['description'],
      (element, next) => {
        //mock the configuration file. Since mockGetUIConfiguration is out of scope of jest.mock(), it must start with 'mock'.
        const mockGetUIConfiguration = getUIConfiguration({
          SORT_EVENTS_BY: element.SORT_EVENTS_BY,
          SORTING_ORDER: element.SORTING_ORDER,
        });
        jest.mock('../../config/ui', () => mockGetUIConfiguration);
        const getSelectionFiltersForMongoQuery = require('../../containers/Logbook/SelectionFilterHelper')
          .getSelectionFiltersForMongoQuery;
        expect(
          getSelectionFiltersForMongoQuery(
            element.criteria,
            { [element.SORT_EVENTS_BY]: element.SORTING_ORDER },
            element.filterTypeCategory
          )
        ).toEqual(element.expected);
        next();
      }
    );
  });

  describe('buildEventFilterForUserComment', () => {
    describe('handle errors', () => {
      it.each(
        resources.buildEventFilterForUserComment,
        '[note: %s ]',
        ['description'],
        (element, next) => {
          const buildEventFilterForUserComment = require('../../containers/Logbook/SelectionFilterHelper')
            .buildEventFilterForUserComment;
          expect(buildEventFilterForUserComment()).toEqual(element.expected);
          next();
        }
      );
    });
  });

  describe('buildSingleEventFilter', () => {
    describe('handle errors', () => {
      it.each(
        resources.buildSingleEventFilter,
        '[note: %s ]',
        ['description'],
        (element, next) => {
          const buildSingleEventFilter = require('../../containers/Logbook/SelectionFilterHelper')
            .buildSingleEventFilter;
          expect(buildSingleEventFilter(element.filter)).toEqual(
            element.expected
          );
          next();
        }
      );
    });
  });

  describe('buildEventFilter', () => {
    describe('handle errors', () => {
      it.each(
        resources.buildEventFilter,
        '[note: %s ]',
        ['description'],
        (element, next) => {
          const buildEventFilter = require('../../containers/Logbook/SelectionFilterHelper')
            .buildEventFilter;
          expect(buildEventFilter(element.filterTypeCategory)).toEqual(
            element.expected
          );
          next();
        }
      );
    });
  });

  describe('buildSearchFilter', () => {
    describe('handle errors', () => {
      it.each(
        resources.buildSearchFilter,
        '[note: %s ]',
        ['description'],
        (element, next) => {
          const buildSearchFilter = require('../../containers/Logbook/SelectionFilterHelper')
            .buildSearchFilter;
          expect(
            buildSearchFilter(element.criteria, element.filterTypeCategory)
          ).toEqual(element.expected);
          next();
        }
      );
    });
  });
});
