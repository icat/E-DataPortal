import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import EventList from '../../components/Logbook/List/EventList';

const resources = require('./resources/eventList.resource.js');

require('it-each')({ testPerIteration: true });

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
});

describe('EventUnitTests', () => {
  describe('Rendering', () => {
    it.each(
      resources.rendering,
      'It renders: %s',
      ['description'],
      (element, next) => {
        const { events, eventCountBySelectionFilter } = element;
        const wrapper = getWrapper({ events, eventCountBySelectionFilter });

        const expected = events !== null && events.length > 0;
        expect(wrapper.exists('Event')).toBe(expected);
        if (events && events.length === 0) {
          expect(wrapper.exists('UserMessage')).toBe(true);
          expect(wrapper.find('UserMessage').prop('message')).toEqual(
            'No log found.'
          );
        }
        next();
      }
    );
  });
});

function getWrapper(props) {
  if (props) {
    const defaultProps = {
      logbookContext: { isReleased: false },
      onEventClicked: () => null,
      onPageClicked: () => null,
    };

    props = { ...defaultProps, ...props };

    return Enzyme.shallow(
      <EventList
        activePage={props.activePage}
        onPageClicked={props.onPageClicked}
        eventCountBySelectionFilter={props.eventCountBySelectionFilter}
        events={props.events}
        logbookContext={props.logbookContext}
        onEventCliked={props.onEventClicked}
      />
    );
  }
}
