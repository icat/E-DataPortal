import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import UserMessage from '../../components/UserMessage';

require('it-each')({ testPerIteration: true });

const resources = require('./resources/UserMessage.resource.js');

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
});

describe('Unit tests on UserMessage', () => {
  function getWrapper(message, type, isTextCentered) {
    return Enzyme.shallow(
      <UserMessage
        isTextCentered={isTextCentered}
        message={message}
        type={type}
      />
    );
  }

  it('renders nothing when no message is provided', () => {
    const actualMessage = '';
    const actualType = 'info';
    expect(getWrapper(actualMessage, actualType).get(0)).toBeNull();
  });

  it.each(resources.rendering, '%s', ['description'], (element, next) => {
    expect(
      getWrapper(element.message, element.type).find('Card').prop('variant')
    ).toEqual(element.expected);
    next();
  });
});
