import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import NewlyAvailableEventsDialogue from '../../components/Logbook/Menu/NewlyAvailableEventsDialogue';

const resources = require('./resources/newlyAvailableEventsDialogue.resource.js');

require('it-each')({ testPerIteration: true });

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
});

describe.skip('NewlyAvailableEventsDialogueUnitTests', () => {
  describe('rendering', () => {
    it.each(resources.rendering, '%s', ['description'], (element, next) => {
      if (element.expected !== null) {
        if (typeof element.expected === 'number') {
          expect(
            getShallowWrapper({
              eventCountSinceLastRefresh: element.eventCountSinceLastRefresh,
              eventCountByPeriodicRefresher:
                element.eventCountByPeriodicRefresher,
              autorefreshEventList: element.autorefreshEventList,
            })
              .find('span')
              .props().children[0]
          ).toEqual(element.expected);
        }
        if (typeof element.expected === 'string') {
          expect(
            getShallowWrapper({
              eventCountSinceLastRefresh: element.eventCountSinceLastRefresh,
              eventCountByPeriodicRefresher:
                element.eventCountByPeriodicRefresher,
              autorefreshEventList: element.autorefreshEventList,
            })
              .find('span')
              .props().children
          ).toEqual(element.expected);
        }
      } else {
        expect(
          getShallowWrapper({
            eventCountSinceLastRefresh: element.eventCountSinceLastRefresh,
            eventCountByPeriodicRefresher:
              element.eventCountByPeriodicRefresher,
            autorefreshEventList: element.autorefreshEventList,
          }).exists('div')
        ).toEqual(false);
      }
      next();
    });
  });

  describe('testPropFunction', () => {
    it('onIconClicked', () => {
      const wrapper = getShallowWrapper({
        eventCountSinceLastRefresh: 0,
        eventCountByPeriodicRefresher:
          resources.testPropFunction.eventCountByPeriodicRefresher,
        onIconClicked: jest.fn(),
      });
      expect(wrapper.instance().props.onIconClicked).toHaveBeenCalledTimes(0);
      wrapper.find('Glyphicon').simulate('click');
      expect(wrapper.instance().props.onIconClicked).toHaveBeenCalledTimes(1);
    });
  });
});

function getShallowWrapper(params) {
  if (params) {
    params.autorefreshEventList = params.autorefreshEventList
      ? params.autorefreshEventList
      : false;
    params.eventCountSinceLastRefresh = params.eventCountSinceLastRefresh
      ? params.eventCountSinceLastRefresh
      : 0;
    params.eventCountByPeriodicRefresher = params.eventCountByPeriodicRefresher
      ? params.eventCountByPeriodicRefresher
      : [];
    params.onIconClicked = params.onIconClicked
      ? params.onIconClicked
      : () => null;

    return Enzyme.shallow(
      <NewlyAvailableEventsDialogue
        autorefreshEventList={params.autorefreshEventList}
        eventCountSinceLastRefresh={params.eventCountSinceLastRefresh}
        eventCountByPeriodicRefresher={params.eventCountByPeriodicRefresher}
        onIconClicked={params.onIconClicked}
      />
    );
  }
}
