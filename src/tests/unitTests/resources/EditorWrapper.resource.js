const { createEventByAttributes } = require('../../helpers');

module.exports = {
  rendering: {
    editor: [
      {
        description: 'create an annotation',
        event: null,
        expected: null,
      },
      {
        description: 'edit an annotation',
        event: createEventByAttributes({
          type: 'annotation',
          content: [{ format: 'html', text: '<p>note to be edited</p>' }],
        }),
        expected: '<p>note to be edited</p>',
      },
      {
        description: 'edit an annotation which has been modified 2 times',
        event: createEventByAttributes({
          type: 'annotation',
          content: [{ format: 'html', text: '<p>the last note</p>' }],
          previousVersionEvent: createEventByAttributes({
            type: 'annotation',
            content: [{ format: 'html', text: '<p>the intermediate note</p>' }],
            previousVersionEvent: createEventByAttributes({
              type: 'annotation',
            }),
          }),
        }),
        expected: '<p>the last note</p>',
      },
      {
        description:
          'edit a notification with no comment, category:commandLine',
        event: createEventByAttributes({
          type: 'notification',
          category: 'commandLine',
        }),
        expected: null,
      },
      {
        description:
          'edit a notification commented 2 times, category:commandLine',
        event: createEventByAttributes({
          type: 'notification',
          category: 'commandLine',
          content: [{ format: 'html', text: '<p>the last comment</p>' }],
          previousVersionEvent: createEventByAttributes({
            type: 'notification',
            category: 'commandLine',
            previousVersionEvent: createEventByAttributes({
              type: 'notification',
              category: 'commandLine',
            }),
          }),
        }),
        expected: '<p>the last comment</p>',
      },
      {
        description: 'edit a notification with no comment, category:error',
        event: createEventByAttributes({
          type: 'notification',
          category: 'error',
        }),
        expected: null,
      },
      {
        description: 'edit a notification commented 2 times, category:error',
        event: createEventByAttributes({
          type: 'notification',
          category: 'error',
          content: [{ format: 'html', text: '<p>the last comment</p>' }],
          previousVersionEvent: createEventByAttributes({
            type: 'notification',
            category: 'error',
            previousVersionEvent: createEventByAttributes({
              type: 'notification',
              category: 'error',
            }),
          }),
        }),
        expected: '<p>the last comment</p>',
      },
      {
        description: 'edit a notification with no comment, category:info',
        event: createEventByAttributes({
          type: 'notification',
          category: 'info',
        }),
        expected: null,
      },
      {
        description: 'edit a notification commented 2 times, category:info',
        event: createEventByAttributes({
          type: 'notification',
          category: 'info',
          content: [{ format: 'html', text: '<p>the last comment</p>' }],
          previousVersionEvent: createEventByAttributes({
            type: 'notification',
            category: 'info',
            previousVersionEvent: createEventByAttributes({
              type: 'notification',
              category: 'info',
            }),
          }),
        }),
        expected: '<p>the last comment</p>',
      },
      {
        description: 'edit a notification with no comment, category:debug',
        event: createEventByAttributes({
          type: 'notification',
          category: 'debug',
        }),
        expected: null,
      },
      {
        description: 'edit a notification commented 2 times, category:debug',
        event: createEventByAttributes({
          type: 'notification',
          category: 'debug',
          content: [{ format: 'html', text: '<p>the last comment</p>' }],
          previousVersionEvent: createEventByAttributes({
            type: 'notification',
            category: 'debug',
            previousVersionEvent: createEventByAttributes({
              type: 'notification',
              category: 'debug',
            }),
          }),
        }),
        expected: '<p>the last comment</p>',
      },
      {
        description: 'edit a notification with no comment, category:comment',
        event: createEventByAttributes({
          type: 'notification',
          category: 'comment',
        }),
        expected: null,
      },
      {
        description: 'edit a notification commented 2 times, category:comment',
        event: createEventByAttributes({
          type: 'notification',
          category: 'comment',
          content: [{ format: 'html', text: '<p>the last comment</p>' }],
          previousVersionEvent: createEventByAttributes({
            type: 'notification',
            category: 'comment',
            previousVersionEvent: createEventByAttributes({
              type: 'notification',
              category: 'comment',
            }),
          }),
        }),
        expected: '<p>the last comment</p>',
      },
    ],
  },
};
