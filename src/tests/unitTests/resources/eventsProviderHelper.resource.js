module.exports = {
  getFirstEventToShowIndex: [
    {
      description: 'page is not provided',
      page: null,
      eventsPerPage: 3,
      expected: 0,
    },
    {
      description: 'eventsPerPage is not provided',
      page: 2,
      eventsPerPage: null,
      expected: 0,
    },

    {
      description: '',
      page: 1,
      eventsPerPage: 3,
      expected: 0,
    },
    {
      description: '',
      page: 2,
      eventsPerPage: 3,
      expected: 3,
    },
    {
      description: '',
      page: 3,
      eventsPerPage: 3,
      expected: 6,
    },
    {
      description: '',
      page: 4,
      eventsPerPage: 3,
      expected: 9,
    },
    {
      description: '',
      page: 5,
      eventsPerPage: 3,
      expected: 12,
    },
  ],
};
