module.exports = {
  rendering: [
    {
      description: 'No rendering when no event are found',
      eventCount: 0,
      EVENTS_PER_PAGE: 10,
      expected: null,
    },
    {
      description: 'No rendering when only one page is needed',
      eventCount: 9,
      EVENTS_PER_PAGE: 10,
      expected: null,
    },
  ],
  getTotalPageNumber: [
    {
      description: 'returns the number of page requested to display the data',
      eventCount: 100,
      EVENTS_PER_PAGE: 10,
      expected: 10,
    },
    {
      description:
        'returns 1 when event count is lower than the config file value',
      eventCount: 20,
      EVENTS_PER_PAGE: 30,
      expected: 1,
    },
    {
      description: 'returns 1 when EVENT_PER_PAGE config value is not set',
      eventCount: 20,
      EVENTS_PER_PAGE: undefined,
      expected: 1,
    },
  ],
};
