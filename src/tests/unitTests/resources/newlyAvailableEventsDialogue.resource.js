module.exports = {
  rendering: [
    {
      description: 'Not autorefreshing event list; 2 new logs arrived',
      eventCountByPeriodicRefresher: 3,
      eventCountSinceLastRefresh: 1,
      expected: 2,
    },
    {
      description: 'Autorefreshing event list',
      autorefreshEventList: true,
      eventCountByPeriodicRefresher: 1,
      eventCountSinceLastRefresh: 0,
      expected: null,
    },
    {
      description: '?',
      eventCountByPeriodicRefresher: 2,
      eventCountSinceLastRefresh: null,
      expected: 2,
    },
    {
      description: 'Renders null case 1',
      eventCountByPeriodicRefresher: null,
      eventCountSinceLastRefresh: null,
      expected: null,
    },
    {
      description: 'Renders null, case 2',
      eventCountByPeriodicRefresher: null,
      eventCountSinceLastRefresh: 0,
      expected: null,
    },
    {
      description: 'Renders null, case 3',
      eventCountByPeriodicRefresher: 3,
      eventCountSinceLastRefresh: 4,
      expected: null,
    },
  ],
  testPropFunction: {
    eventCountByPeriodicRefresher: 1,
  },
};
