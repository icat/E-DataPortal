import { LOGBOOK_CONTAINER_CONTEXT } from '../../../constants/eventTypes';
import { goodComment1, goodComment4 } from '../../GoodEventLibrary';
import { createEventByAttributes } from '../../helpers';

module.exports = {
  renderinglinkToEditEvent: [
    {
      description: 'logbook is not public; event has no previous version',
      event: goodComment1,
      logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
      expected: 'pencil',
    },
    {
      description: 'logbook is not public; event has one previous version',
      event: goodComment4,
      logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
      expected: 'pencil',
    },
    {
      description: 'logbook is public',
      event: goodComment4,
      logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },
      expected: 'eye-open',
    },
  ],
  renderingEventTextBox: [
    {
      description: 'an annotation',
      event: createEventByAttributes({ type: 'annotation' }),
    },
    {
      description: 'a notification, info',
      event: createEventByAttributes({
        type: 'notification',
        category: 'info',
      }),
    },
    {
      description: 'a notification, error',
      event: createEventByAttributes({
        type: 'notification',
        category: 'error',
      }),
    },
    {
      description: 'a notification, commandline',
      event: createEventByAttributes({
        type: 'notification',
        category: 'commandline',
      }),
    },
    {
      description: 'a notification, debug',
      event: createEventByAttributes({
        type: 'notification',
        category: 'debug',
      }),
    },
  ],
};
