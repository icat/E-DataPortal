import { LOGBOOK_CONTAINER_CONTEXT } from '../../../constants/eventTypes';

module.exports = {
  rendering: {
    eventList: [
      {
        description: 'logbook is not public, has no events',
        logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
        getEventCountResponse: { data: '0' },
        expected: true,
      },
      {
        description: 'logbook is not public, has events',
        logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
        getEventCountResponse: { data: '1' },
        expected: true,
      },
      {
        description: 'logbook is public, has no events',
        logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },
        getEventCountResponse: { data: '0' },
        expected: true,
      },
      {
        description: 'logbook is public, has events',
        logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },
        getEventCountResponse: { data: '1' },
        expected: true,
      },
    ],
  },
};
