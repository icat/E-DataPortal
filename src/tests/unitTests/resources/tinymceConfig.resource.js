module.exports = {
  toolbar: [
    {
      description: 'toolbar for mobile screens',
      width: 400,
      expected: 'undo redo paste | bold italic | bullist | image',
    },
    {
      description: 'toolbar for mobile screens',
      width: 850,
      expected: 'undo redo paste | bold italic | bullist | image',
    },
    {
      description: 'toolbar for large screens (>768px)',
      width: 1980,
      expected:
        'undo redo | fontsizeselect bold italic | charmap | subscript superscript | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | link image table | fullscreen',
    },
    {
      description: 'toolbar for large screens (>768px)',
      width: 1051,
      expected:
        'undo redo | fontsizeselect bold italic | charmap | subscript superscript | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | link image table | fullscreen',
    },
  ],
};
