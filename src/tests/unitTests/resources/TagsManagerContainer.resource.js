module.exports = {
  rendering: [
    {
      description: 'No available tags',
      tags: [],
      logbookContext: { name: null, isReleased: false },
    },
  ],
};
