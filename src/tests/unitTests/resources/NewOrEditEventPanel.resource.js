import {
  EDIT_EVENT_CONTEXT,
  NEW_EVENT_CONTEXT,
} from '../../../constants/eventTypes';
import { createEventByAttributes } from '../../helpers';

module.exports = {
  renderingEventHeader: [
    {
      description: 'create an event',
      event: null,
      expected: 'New comment',
    },
    {
      description: 'edit an event',
      event: createEventByAttributes({}),
      expected: 'Edit',
    },
  ],
  renderingEditorWrapper: [
    {
      description:
        'check all none functional props are passed to EditorWrapper for event creation',
      event: null,
      investigationId: '0',
      user: { sessionId: 'testSessionId', username: 'testUsername' },
    },
    {
      description:
        'check all none functional props are passed to EditorWrapper for event edition',
      event: createEventByAttributes({}),
      investigationId: '0',
      user: { sessionId: 'testSessionId', username: 'testUsername' },
    },
  ],
  renderingAuthorAndTime: [
    {
      description:
        'check none functional props are passed to AuthorAndTime panel for new event creation',
      event: null,
    },
    {
      description:
        'check none functional props are passed to CreationDate panel for event edition',
      event: createEventByAttributes({}),
    },
  ],
  renderingTagContainer: [
    {
      description:
        'check none functional props are passed to TagContainer panel for new event creation',
      event: null,
      investigationId: '0',
    },
    {
      description:
        'check none functional props are passed to EventHistoryLabel panel for event edition',
      event: createEventByAttributes({}),
      investigationId: '0',
    },
  ],
  renderingEventFooter: [
    {
      description: 'check eventFooter is rendered for new event creation',
      context: NEW_EVENT_CONTEXT,
    },
    {
      description: 'check eventFooter is rendered for event edition',
      context: EDIT_EVENT_CONTEXT,
    },
  ],
  testFunctionalProps: {
    investigationId: '0',
    event: createEventByAttributes({ username: 'testUsername' }),
    user: { sessionId: 'sessionId' },
  },
};
