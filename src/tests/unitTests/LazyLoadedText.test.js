import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import LazyLoadedText from '../../components/Logbook/List/LazyLoadedText';

const resources = require('./resources/lazyLoadedText.resource.js');

require('it-each')({ testPerIteration: true });

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
});

describe.skip('LazyLoadedTextUnitTests', () => {
  describe('Rendering', () => {
    it.each(resources.rendering, '%s', ['description'], (element, next) => {
      const wrapper = getWrapper({ text: element.text });

      expect(wrapper.find('LazyLoad').exists()).toBe(
        element.text.indexOf('<img') === -1
      );
      next();
    });
  });
});

function getWrapper(params) {
  return Enzyme.shallow(<LazyLoadedText text={params.text} />);
}
