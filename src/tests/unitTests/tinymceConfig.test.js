import { EditionModeConfig } from '../../components/Logbook/Editor/tinymceConfig';

require('it-each')({ testPerIteration: true });
const resources = require('./resources/tinymceConfig.resource.js');

describe('tinymceConfigUnitTests', () => {
  it.each(resources.toolbar, '%s', ['description'], (element, next) => {
    const config = new EditionModeConfig(element.width);
    expect(config.toolbar).toEqual(element.expected);
    next();
  });
});
