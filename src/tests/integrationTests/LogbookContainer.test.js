import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import JestMockPromise from 'jest-mock-promise';
import React from 'react';
import { applyMiddleware, createStore } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import { LOGGED_IN } from '../../constants/actionTypes';
import { getText } from '../../helpers/eventHelpers';
import reducer from '../../reducers';
import getUIConfiguration from '../uiConfig';

const resources = require('./resources/LogbookContainer.resource.js');

require('it-each')({ testPerIteration: true });

beforeEach(() => {
  Enzyme.configure({ adapter: new Adapter() });
  jest.resetModules();
  jest.useFakeTimers();
});

describe.skip('LogbookContainerIntegrationTests', () => {
  describe('rendering', () => {
    it.each(
      resources.rendering.panels,
      '%s',
      ['description'],
      (element, next) => {
        const mockGetUIConfiguration = getUIConfiguration({
          AUTOREFRESH_EVENTLIST: false,
        });
        jest.mock('../../config/ui', () => mockGetUIConfiguration);

        // require logbook class after the configuration mocking
        const LogbookContainerClass = require('../../containers/Logbook/LogbookContainer')
          .LogbookContainerClass;
        const getEventsBySelectionFilterPromise = new JestMockPromise(
          (resolve) => resolve(element.getEventsBySelectionFilterResponse1)
        );
        const getEventCountBySelectionFilterPromise = new JestMockPromise(
          (resolve) => resolve(element.getEventCountBySelectionFilterResponse1)
        );

        LogbookContainerClass.prototype.getEventsBySelectionFilter = () =>
          getEventsBySelectionFilterPromise;
        LogbookContainerClass.prototype.getEventCountBySelectionFilter = () =>
          getEventCountBySelectionFilterPromise;

        const wrapper = getMountedWrapper({
          investigationId: element.investigationId,
          releasedInvestigations: element.releasedInvestigations,
        });
        getEventsBySelectionFilterPromise.resolve();
        getEventCountBySelectionFilterPromise.resolve();

        wrapper.update();

        // It renders the animation widget first
        expect(wrapper.find('Loading')).toBeDefined();

        wrapper.find('EventList').find('Button').simulate('click');
        jest.advanceTimersByTime(100); // advance the timer included in the react-responsive-modal despite its animationDuration prop set to 0
        wrapper.update();
        expect(wrapper.find('OverlayBox').at(1).exists(element.expected)).toBe(
          true
        );
        next();
      }
    );
  });

  describe('flipEventsSortingOrder', () => {
    it.each(
      resources.flipEventsSortingOrder,
      '%s',
      ['description'],
      (element, next) => {
        //mock the configuration file. Since mockGetUIConfiguration is out of scope of jest.mock(), it must start with 'mock'.
        const mockGetUIConfiguration = getUIConfiguration({
          SORT_EVENTS_BY: element.SORT_EVENTS_BY,
          SORTING_ORDER: element.SORTING_ORDER,
          AUTOREFRESH_EVENTLIST: false,
        });
        jest.mock('../../config/ui', () => mockGetUIConfiguration);

        // require logbook class after the configuration mocking
        const LogbookContainerClass = require('../../containers/Logbook/LogbookContainer')
          .LogbookContainerClass;
        LogbookContainerClass.prototype.getEventsBySelectionFilter = jest.fn(
          () => Promise.resolve(element.serverResponse_No_Event)
        );
        LogbookContainerClass.prototype.getEventCountBySelectionFilter = jest.fn(
          () => Promise.resolve(element.serverResponse_No_EventCount)
        );
        expect(
          LogbookContainerClass.prototype.getEventsBySelectionFilter
        ).toHaveBeenCalledTimes(0);

        const mountedWrapper = getMountedWrapper({});
        mountedWrapper.update();
        expect(
          LogbookContainerClass.prototype.getEventsBySelectionFilter
        ).toHaveBeenCalledTimes(1);
        expect(
          LogbookContainerClass.prototype.getEventsBySelectionFilter
        ).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          element.expected.sortingFilterArgAtfirstCall
        );

        mountedWrapper
          .find('EventListMenu')
          .find({ eventKey: element.eventKey })
          .find('a')
          .simulate('click');

        // check the event list is flipped ie the proper call to the server is done; offset is 0 (ie show first page)
        expect(
          LogbookContainerClass.prototype.getEventsBySelectionFilter
        ).toHaveBeenCalledTimes(2);
        expect(
          LogbookContainerClass.prototype.getEventsBySelectionFilter
        ).toHaveBeenCalledWith(
          0,
          expect.anything(),
          element.expected.sortingFilterArgAtSecondCall
        );

        next();
      }
    );
  });

  describe('autorefresh', () => {
    it.each(
      resources.autorefresh,
      '%s',
      ['description'],
      async (element, next) => {
        // Mock the configuration file. Since mockGetUIConfiguration is out of scope of jest.mock(), it must start with 'mock'.
        const mockGetUIConfiguration = getUIConfiguration({
          SORT_EVENTS_BY: element.SORT_EVENTS_BY,
          SORTING_ORDER: element.SORTING_ORDER,
          AUTOREFRESH_EVENTLIST: true,
          AUTOREFRESH_DELAY: element.AUTOREFRESH_DELAY,
        });
        jest.mock('../../config/ui', () => mockGetUIConfiguration);

        // require logbook class after the configuration mocking
        const LogbookContainerClass = require('../../containers/Logbook/LogbookContainer')
          .LogbookContainerClass;
        // the first requests triggered by bothe the logbook at initialization and then periodic refresher
        let getEventsBySelectionFilterPromise = new JestMockPromise((resolve) =>
          resolve(element.getEventsBySelectionFilterResponse1)
        );

        // Initialize LogbookContainer
        LogbookContainerClass.prototype.getEventsBySelectionFilter = () =>
          getEventsBySelectionFilterPromise;
        LogbookContainerClass.prototype.getEventCountBySelectionFilter = () =>
          Promise.resolve(element.getEventCountBySelectionFilterResponse1);
        const mountedWrapper = getMountedWrapper({});
        getEventsBySelectionFilterPromise.resolve(); // manually trigger 'then' in componentDidMount()

        // Test EventList is rendered
        mountedWrapper.update();
        expect(
          getText(
            mountedWrapper.find('Event').at(0).prop('event').content,
            'plainText'
          )
        ).toEqual(element.expected.firstEventInListBeforeClick);

        // Mimics a call triggered by the periodic refresher which does not resolve immediately (pending request)
        const getEventsBySelectionFilterPromise2 = new JestMockPromise(
          (resolve) => {
            setTimeout(() => {
              return resolve(element.getEventsBySelectionFilterResponse1);
            }, element.AUTOREFRESH_DELAY + 500);
          }
        );
        mountedWrapper
          .find('LogbookContainer')
          .instance().getEventsBySelectionFilter = () =>
          getEventsBySelectionFilterPromise2;
        await console.log(
          'Triggers periodic refresh timers and only the first timer of the promise'
        );
        //await jest.runOnlyPendingTimers(); // Triggers periodic refresh timers and only the first timer of the promise
        jest.runTimersToTime(element.AUTOREFRESH_DELAY);

        // User clicks to sort oldest event first
        await console.log('user clicks');
        getEventsBySelectionFilterPromise = new JestMockPromise((resolve) =>
          resolve(element.getEventsBySelectionFilterResponse2)
        );
        mountedWrapper
          .find('LogbookContainer')
          .instance().getEventsBySelectionFilter = () =>
          getEventsBySelectionFilterPromise;
        await mountedWrapper
          .find('EventListMenu')
          .find({ eventKey: element.eventKey })
          .find('a')
          .simulate('click');
        await getEventsBySelectionFilterPromise.resolve(); // manually trigger 'then' in flipEventSortingOrder()

        // The event list is rendered. Check that oldest event is before youngest event
        await console.log('check render after clicking');
        await mountedWrapper.update();
        await expect(
          getText(
            mountedWrapper.find('Event').at(0).prop('event').content,
            'plainText'
          )
        ).toEqual(element.expected.firstEventInListAfterClick);

        // Mimic the pending call from periodic refresher has received its data
        await console.log(
          'Triggers periodic refresh timer and the second timer of the promise'
        );
        jest.runTimersToTime(500);
        await getEventsBySelectionFilterPromise2.resolve(); // manually trigger 'then' in onNewEventCountReceived()

        // Test that the Eventist is unchanged
        await console.log('check render a last');
        await mountedWrapper.update();
        await expect(
          getText(
            mountedWrapper.find('Event').at(0).prop('event').content,
            'plainText'
          )
        ).toEqual(element.expected.firstEventInListAfterClick);

        next();
      }
    );
  });
});

// describe("onNewEventUploaded", () => {

//     let mockedGetEvents = null;

//     beforeEach(() => {
//         // mock getEvents to avoid verbose http request errors
//         mockedGetEvents = jest.spyOn(LogbookContainer.prototype, 'getEvents').mockImplementation(() => null);
//     })
//     afterEach(() => {
//         mockedGetEvents.mockRestore()
//     })

//     function getWrapper() {
//         /* overrides default configuration */
//         GUI_CONFIG.mockReturnValue({
//             EVENTS_PER_PAGE: 2,
//             DEFAULT_SORTING_FILTER: {
//                 createdAt: -1  // temporary workaround. createdAt must be replaced by creationDate
//             },
//             AUTOREFRESH_DELAY: 100,

//         })

//         const middleware = [thunk];
//         const persistConfig = {
//             key: 'root',
//             storage,
//         }
//         const persistedReducer = persistReducer(persistConfig, reducer)
//         const store = createStore(
//             persistedReducer,
//             {
//                 user: {
//                     type: LOGGED_IN,
//                     username: 'username',
//                     sessionId: 'testSessionId'
//                 }
//             },
//             applyMiddleware(...middleware, logger, promise(), thunk)
//         )

//         return (Enzyme.shallow(<LogbookContainerInConnect
//             investigationId="testInvestigationId"
//             store={store}
//         />).dive())
//     }

//     function createEvent(id, title, creationDate) {
//         return ({
//             _id: id,
//             category: "comment",
//             content: [{
//                 format: 'plainText',
//                 text: 'this is a test'
//             }, {
//                 format: 'html',
//                 text: '<p> this is a test </p>'
//             }],
//             createdAt: creationDate, // was "2018-01-01T00:00:00.000Z"
//             creationDate: creationDate,
//             datasetId: null,
//             file: [],
//             fileSize: null,
//             filename: null,
//             investigationId: 'testInvestigationId',
//             machine: null,
//             previousVersionEvent: null,
//             software: null,
//             tag: [],
//             title: title,
//             type: "annotation",
//             updatedAt: "2018-01-01T00:00:00.000Z",
//             username: "mchaille"
//         })
//     }

//     function setupWrapper(wrapper, activePage, numberEventsFoundOnServer, numberEventsDownloaded) {
//         function getSliceForPageEvent() {
//             if (activePage === 1) {
//                 return actualfoundEvents.slice(0, 2)
//             }
//             if (activePage === 2) {
//                 return actualfoundEvents.slice(2, 4)
//             }
//             if (activePage === 3) {
//                 return actualfoundEvents.slice(4, 6)
//             }
//             if (activePage === 4) {
//                 return actualfoundEvents.slice(6, 8)
//             }
//             if (activePage === 5) {
//                 return actualfoundEvents.slice(8, 10)
//             }
//             if (activePage === 6) {
//                 return actualfoundEvents.slice(10)
//             }
//         }

//         function getSliceForDownloadedEvent() {
//             if (activePage === 1) {
//                 return actualfoundEvents.slice(0, 4)
//             }
//             if (activePage === 2) {
//                 return actualfoundEvents.slice(2, 6)
//             }
//             if (activePage === 3) {
//                 return actualfoundEvents.slice(4, 8)
//             }
//             if (activePage === 4) {
//                 return actualfoundEvents.slice(6, 10)
//             }
//             if (activePage === 5) {
//                 return actualfoundEvents.slice(8, 11)
//             }
//             if (activePage === 6) {
//                 return actualfoundEvents.slice(10)
//             }
//         }

//         function getDownloadedEventIndexes() {
//             if (activePage === 1) {
//                 return { start: 0, end: 3 }
//             }
//             if (activePage === 2) {
//                 return { start: 2, end: 5 }
//             }
//             if (activePage === 3) {
//                 return { start: 4, end: 7 }
//             }
//             if (activePage === 4) {
//                 return { start: 6, end: 9 }
//             }
//             if (activePage === 5) {
//                 return { start: 8, end: 10 }
//             }
//             if (activePage === 6) {
//                 return { start: 10, end: 10 }
//             }
//         }

//         /* creates numberEventsFoundOnServer events */
//         let actualfoundEvents = new Array();
//         for (let index = 1; index <= numberEventsFoundOnServer; index++) {
//             actualfoundEvents.push(createEvent(index, "title" + index, new Date('0' + index + ' Jan 2018 00:00:00 GMT')));
//         }

//         let actualActivePage = activePage;

//         // setup the starting state of the instance
//         wrapper.instance().setState({
//             activePage: actualActivePage,
//             downloadedEvents: getSliceForDownloadedEvent(),
//             downloadedEventIndexes: getDownloadedEventIndexes(),
//             foundEventCount: numberEventsFoundOnServer,
//             pageEvents: getSliceForPageEvent()
//         })
//         wrapper.update();

//         // mock the downloadEvent functions, because we want to see how it is called
//         wrapper.instance().getEvents = jest.fn();

//         wrapper.update();
//     }

//     it("sets the activepage to 1", () => {
//         let myEventContainerWrapper = getWrapper();

//         // create a wrapper that has received 4 events (while there are 11 found by the
//         // server) and currently displays page 2
//         let actualActivePage = 2;
//         setupWrapper(myEventContainerWrapper, actualActivePage, 11, 4)
//         expect(myEventContainerWrapper.instance().state.activePage).toBe(2);

//         // user has created  a new Event
//         myEventContainerWrapper.instance().onNewEventUploaded();

//         expect(myEventContainerWrapper.instance().state.activePage).toBe(1);
//     })

//     it("sets sets isNewEventVisible to false", () => {
//         let myEventContainerWrapper = getWrapper();

//         // create a wrapper that has received 4 events (while there are 11 found by the
//         // server) and currently displays page 2
//         let actualActivePage = 2;
//         setupWrapper(myEventContainerWrapper, actualActivePage, 11, 4)
//         myEventContainerWrapper.instance().state.isNewEventVisible = true;

//         // user has created  a new Event
//         myEventContainerWrapper.instance().onNewEventUploaded();

//         expect(myEventContainerWrapper.instance().state.isNewEventVisible).toBe(false);
//     })
// })

/**
 * Get a mounted wrapper
 */
function getMountedWrapper(propsObject) {
  const middleware = [thunk];
  const persistConfig = { key: 'root', storage };
  const persistedReducer = persistReducer(persistConfig, reducer);
  const store = createStore(
    persistedReducer,
    {
      user: {
        type: LOGGED_IN,
        username: 'username',
        sessionId: 'testSessionId',
      },
      releasedInvestigations: propsObject.releasedInvestigations
        ? { data: propsObject.releasedInvestigations }
        : { data: null },
    },
    applyMiddleware(...middleware, promise(), thunk)
  );
  const LogbookContainer = require('../../containers/Logbook/LogbookContainer')
    .default;
  return Enzyme.mount(
    <LogbookContainer
      investigationId={
        propsObject.investigationId
          ? propsObject.investigationId
          : 'testInvestigationId'
      }
    />,
    { context: { store } }
  );
}
