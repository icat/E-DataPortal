import { createEventByAttributes } from '../../helpers';

module.exports = {
  rendering: {
    panels: [
      {
        description:
          'renders PublicEventPanel when logbook is released and user clicks on the button to see details of the event',
        getEventsBySelectionFilterResponse1: [createEventByAttributes({})],
        getEventCountBySelectionFilterResponse1: 1,
        investigationId: 'myInvestigationId',
        releasedInvestigations: [{ id: 'myInvestigationId' }],
        expected: 'PublicEventPanel',
      },
      {
        description:
          'renders the NewOrEditEventPanel when logbook is not released and user clicks on the edit button',
        getEventsBySelectionFilterResponse1: [createEventByAttributes({})],
        getEventCountBySelectionFilterResponse1: 1,
        investigationId: 'myInvestigationId',
        releasedInvestigations: [],
        expected: 'NewOrEditEventPanel',
      },
    ],
    // renderAnimationFirstAndThenEventList: {
    //     serverResponse1: {
    //         data: [{
    //             _id: 'testId1',
    //             category: "comment",
    //             content: [
    //                 { format: 'plainText', text: 'this is a test' },
    //                 { format: 'html', text: '<p> this is a test </p>' }
    //             ],
    //             createdAt: '2018-01-01T00:01:00.000Z',
    //             creationDate: '2018-01-01T00:00:00.000Z',
    //             datasetId: null,
    //             file: [],
    //             fileSize: null,
    //             filename: null,
    //             investigationId: 'testInvestigationId',
    //             machine: null,
    //             previousVersionEvent: null,
    //             software: null,
    //             tag: [],
    //             title: 'test title',
    //             type: "annotation",
    //             updatedAt: "2018-01-01T00:00:00.000Z",
    //             username: "mchaille"
    //         }]
    //     },
    //     serverResponse2: { data: '1' },
    // }
  },

  //     ,{
  //     renderingPublicEventPanel: [
  //         {
  //             description: 'logbook is public',
  //             logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },

  //             expected: true
  //         },
  //         {
  //             description: 'logbook is not public',
  //             logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
  //             getEventsBySelectionFilterResponse1: [createEventByAttributes({})],
  //             getEventCountBySelectionFilterResponse1: { data: '1' },
  //             expected: false
  //         }
  //     ]
  // }
  //     },
  flipEventsSortingOrder: [
    {
      description: 'sortOldestEventsFirst',
      serverResponse_No_Event: [],
      serverResponse_No_EventCount: { data: '0' },
      SORT_EVENTS_BY: '_id',
      SORTING_ORDER: -1,
      eventKey: 3.3,
      expected: {
        sortingFilterArgAtfirstCall: { _id: -1 },
        sortingFilterArgAtSecondCall: { _id: 1 },
      },
    },
    {
      description: 'sortYoungestEventsFirst',
      serverResponse_No_Event: [],
      serverResponse_No_EventCount: { data: '0' },
      SORT_EVENTS_BY: '_id',
      SORTING_ORDER: 1,
      eventKey: 3.4,
      expected: {
        sortingFilterArgAtfirstCall: { _id: 1 },
        sortingFilterArgAtSecondCall: { _id: -1 },
      },
    },
    {
      description: 'sortOldestEventsFirst',
      serverResponse_No_Event: [],
      serverResponse_No_EventCount: { data: '0' },
      SORT_EVENTS_BY: 'creationDate',
      SORTING_ORDER: -1,
      eventKey: 3.3,
      expected: {
        sortingFilterArgAtfirstCall: { creationDate: -1 },
        sortingFilterArgAtSecondCall: { creationDate: 1 },
      },
    },
    {
      description: 'sortYoungestEventsFirst',
      serverResponse_No_Event: [],
      serverResponse_No_EventCount: { data: '0' },
      SORT_EVENTS_BY: 'creationDate',
      SORTING_ORDER: 1,
      eventKey: 3.4,
      expected: {
        sortingFilterArgAtfirstCall: { creationDate: 1 },
        sortingFilterArgAtSecondCall: { creationDate: -1 },
      },
    },
  ],
  autorefresh: [
    {
      description:
        'When user changes the sorting order, it does not automatically refresh the eventList from a previously initiated request',
      SORT_EVENTS_BY: 'creationDate',
      SORTING_ORDER: -1, //youngest first
      AUTOREFRESH_DELAY: 1000,
      eventKey: 3.3,
      getEventsBySelectionFilterResponse1: [
        createEventByAttributes({
          _id: 'testId1',
          content: [
            { format: 'plainText', text: 'youngest event' },
            { format: 'html', text: '<p> text </p>' },
          ],
          createdAt: '2018-01-02T00:01:00.000Z',
          creationDate: '2018-01-02T00:00:00.000Z',
          updatedAt: '2018-01-02T00:00:00.000Z',
        }),
        createEventByAttributes({
          _id: 'testId2',
          content: [
            { format: 'plainText', text: 'oldest event' },
            { format: 'html', text: '<p> oldest event </p>' },
          ],
          createdAt: '2018-01-01T00:01:00.000Z',
          creationDate: '2018-01-01T00:00:00.000Z',
          updatedAt: '2018-01-01T00:00:00.000Z',
        }),
      ],
      getEventCountBySelectionFilterResponse1: 2,
      getEventsBySelectionFilterResponse2: [
        createEventByAttributes({
          _id: 'testId2',
          content: [
            { format: 'plainText', text: 'oldest event' },
            { format: 'html', text: '<p> oldest event </p>' },
          ],
          createdAt: '2018-01-01T00:01:00.000Z',
          creationDate: '2018-01-01T00:00:00.000Z',
          updatedAt: '2018-01-01T00:00:00.000Z',
        }),
        createEventByAttributes({
          _id: 'testId1',
          content: [
            { format: 'plainText', text: 'youngest event' },
            { format: 'html', text: '<p> text </p>' },
          ],
          createdAt: '2018-01-02T00:01:00.000Z',
          creationDate: '2018-01-02T00:00:00.000Z',
          updatedAt: '2018-01-02T00:00:00.000Z',
        }),
      ],
      expected: {
        firstEventInListBeforeClick: 'youngest event',
        firstEventInListAfterClick: 'oldest event',
      },
    },
  ],
};
