import { createEventByAttributes } from '../../helpers';

module.exports = {
  functionalProps: {
    event: createEventByAttributes({}),
  },
};
