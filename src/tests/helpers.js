/** Create an event by attributes.
 * @param {object} attributesObj. An object containing one or several of the attribute the event will have. Example: {category: 'error'}. Full description of the available
 * properties [here](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/app/models/event.model.js)
 * @return {object} Event with provided attributes.
 * Attributes which are not provided are set by default as shown below:
 * _id : 'defaultId',
 * category: 'comment',
 * content : [ { format: 'plainText', text: 'default content' }, { format: 'html', text: '<p> default content </p>' }],
 * createdAt: '2018-01-01T00:00:01.000Z',
 * creationDate : '2018-01-01T00:00:00.000Z',
 * datasetId: null,
 * file : [],
 * fileSize: null,
 * filename : null,
 * investigationId : 'defaultInvestigationId',
 * machine: null,
 * previousVersionEvent : null,
 * software : null,
 * tag : [],
 * title : null,
 * type : 'annotation',
 * updatedAt : "2018-01-01T00:00:01.000Z";
 * username : null,
 */
export function createEventByAttributes(attributesObj) {
  return {
    _id: attributesObj._id ? attributesObj._id : 'defaultId',
    category: attributesObj.category ? attributesObj.category : 'comment',
    content: attributesObj.content
      ? attributesObj.content
      : [
          { format: 'plainText', text: 'default content' },
          { format: 'html', text: '<p> default content </p>' },
        ],
    createdAt: attributesObj.createdAt
      ? attributesObj.createdAt
      : '2018-01-01T00:00:01.000Z',
    creationDate: attributesObj.creationDate
      ? attributesObj.creationDate
      : '2018-01-01T00:00:00.000Z',
    datasetId: attributesObj.datasetId ? attributesObj.datasetId : null,
    file: attributesObj.file ? attributesObj.file : [],
    fileSize: attributesObj.fileSize ? attributesObj.fileSize : null,
    filename: attributesObj.filename ? attributesObj.filename : null,
    investigationId: attributesObj.investigationId
      ? attributesObj.investigationId
      : 'defaultInvestigationId',
    machine: attributesObj.machine ? attributesObj.machine : null,
    previousVersionEvent: attributesObj.previousVersionEvent
      ? attributesObj.previousVersionEvent
      : null,
    software: attributesObj.software ? attributesObj.software : null,
    tag: attributesObj.tag ? attributesObj.tag : [],
    title: attributesObj.title ? attributesObj.title : null,
    type: attributesObj.type ? attributesObj.type : 'annotation',
    updatedAt: attributesObj.updatedAt
      ? attributesObj.updatedAt
      : '2018-01-01T00:00:01.000Z',
    username: attributesObj.username ? attributesObj.username : null,
    meta: { page: { currentPage: 1, total: 5, totalPages: 1 } },
  };
}

/**
 * Create a list of events. Each one has its own _id and the event content equals _id.
 * @param {Number} firstId id of the first event to be created
 * @param {Number} lastId id of the last event to be created
 * @returns {Array} events
 */
export function createEventsWithContentEqualsId(firstId, lastId) {
  const events = [];
  for (let index = firstId; index <= lastId; index++) {
    events.push(
      createEventByAttributes({
        _id: String(index),
        meta: { page: { currentPage: 1, total: 5, totalPages: 1 } },
        content: [
          { format: 'plainText', text: String(index) },
          { format: 'html', text: `<p>${String(index)}</p>` },
        ],
      })
    );
  }

  return events;
}

/**
 * Create events with empty content
 * @param {Number} count number of events to be created
 * @returns {Array} events
 */
export function createEventsWithEmptyContent(count) {
  const events = [];
  for (let i = 1; i <= count; i++) {
    events.push(
      createEventByAttributes({
        _id: String(i),
        meta: { page: { currentPage: 1, total: 5, totalPages: 1 } },
        content: [
          { format: 'plainText', text: '' },
          { format: 'html', text: '<p></p>' },
        ],
      })
    );
  }

  return events;
}
