# Data Portal

The Data Portal is a web interface that allows users to browse and download their data and public data.

Check the [latest documentation](https://icat.gitlab-pages.esrf.fr/E-DataPortal/)
