FROM node:12-alpine as build
RUN apk add --no-cache tzdata
ENV TZ=Europe/Paris


ENV NODE_ENV production

COPY . /edataportal
WORKDIR /edataportal

RUN apk --no-cache add git \
  && npm ci \
  && npm run build


FROM nginx:alpine

COPY --from=build /edataportal/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

RUN apk --no-cache add curl
COPY ./healthcheck /

EXPOSE 80

HEALTHCHECK --start-period=10s --timeout=5s --retries=3 CMD /healthcheck
